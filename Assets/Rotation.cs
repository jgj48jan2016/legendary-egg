﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {

    public Vector3 v2Rot;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(v2Rot.x * Time.deltaTime, v2Rot.y * Time.deltaTime, v2Rot.z * Time.deltaTime);
	}
}
