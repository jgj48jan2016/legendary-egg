﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.UI;
//using System;

public class Level1 : MonoBehaviour {

    public Text txLife;
    public Text txScore;
    public Text txBestScore;
   // public Text txPlayer;
   // public Text txTimeLogin;
    public Text txYourScore;

    SoundManager soundM;
    StatesManagement stateM;
    
    public GameObject[] pivot;
    public GameObject prefabEnemy;
    public GameObject prefabEnemy2;
    public GameObject prefabEnemy3;
    private GameObject[] goEnemy;
    public static bool isDestroyEnemy = false;
    public GameObject target;
    public GameObject particleDestroy;
    public GameObject infoBestScore;
//	public Text scoreText;
//	public GameObject anim;
//	private Animator scorePanel;
//	public GameObject tuts;

    //[SerializePrivateVariables]
    public static bool[] isShowEnemy;
    private float[] timerStanbyEnemy;
    private int[] condition;
    private float[] timerTouch;
	public static bool isLose = false;
    //MenuUI gui;

    public static int score = 0;
    public static int life = 5;
    public static int gameover = 0;
     int ibestScore = 0;
       int iplayer = 0;
	// Use this for initialization
	void Start () {
        ibestScore = PlayerPrefs.GetInt("best", 0);
        txBestScore.text = ibestScore.ToString();
        iplayer = PlayerPrefs.GetInt("player", 0);
        isShowEnemy = new bool[9];
        goEnemy = new GameObject[9];
        timerStanbyEnemy = new float[9];
        condition = new int[9];
        timerTouch = new float[9];
		isLose = false;

        stateM = GetComponent<StatesManagement>();
        stateM.setState(0);
        gameover = 0;
        
        soundM = GetComponent<SoundManager>();

       // txTimeLogin.text = "";
        //spownEnemy();

    }



    private void spownEnemy()
    {


        int iEnemy = (int)UnityEngine.Random.Range(0, 9);
        if (!isShowEnemy[iEnemy])
        {
            
            timerStanbyEnemy[iEnemy] = 0;
            isShowEnemy[iEnemy] = true;
            if(iEnemy == 0 || iEnemy == 3 || iEnemy == 6)
            {
                goEnemy[iEnemy] = (GameObject)Instantiate(prefabEnemy, pivot[iEnemy].transform.position, pivot[iEnemy].transform.rotation);
                goEnemy[iEnemy].transform.localEulerAngles = new Vector3(0, 0, 0);
            }
            else if (iEnemy == 1 || iEnemy == 4 || iEnemy == 7)
            {
                goEnemy[iEnemy] = (GameObject)Instantiate(prefabEnemy2, pivot[iEnemy].transform.position, pivot[iEnemy].transform.rotation);
                goEnemy[iEnemy].transform.localEulerAngles = new Vector3(0, 0, 270);
            }
            else if (iEnemy == 2 || iEnemy == 5 || iEnemy == 8)
            {
                goEnemy[iEnemy] = (GameObject)Instantiate(prefabEnemy3, pivot[iEnemy].transform.position, pivot[iEnemy].transform.rotation);
                goEnemy[iEnemy].transform.localEulerAngles = new Vector3(0, 0, 180);
            }
                
            goEnemy[iEnemy].transform.localScale = new Vector3(0, 0, 1);
            goEnemy[iEnemy].GetComponent<Animator>().SetBool("isSpown",true);
            //goEnemy[iEnemy].GetComponent<Animator>().SetBool("isSpown", false);
            goEnemy[iEnemy].name = "enemy_" + iEnemy;
            if(!soundM.asNagaTerbang.isPlaying && StatesManagement.isPlayMusik == 1)
            soundM.asNagaTerbang.Play();
            condition[iEnemy] = 1;
            
        }
    }
    private bool isClickPlay = false;
    public void OnClickPlay()
    {
        isClickPlay = true;
        stateM.setState(3);
    }

    private void moveToEgg(int idx)
    {
        if (isShowEnemy[idx] && condition[idx] != 2)
        {
            timerStanbyEnemy[idx] += Time.deltaTime;
            if (timerStanbyEnemy[idx] > 1)
            {
               // if (!soundM.asNagaTerbang.isPlaying)
              //  {
                   
             //   }
                float dX = target.transform.position.x - goEnemy[idx].transform.position.x;
                float dY = target.transform.position.y - goEnemy[idx].transform.position.y;

                float rad = Mathf.Atan2(dY, dX);
                float deg = rad * Mathf.Rad2Deg;

                goEnemy[idx].transform.localEulerAngles = new Vector3(0, 0, deg);

                // if (!goEnemy[idx].GetComponent<Animator>().GetBool("isFly"))
                // {
                goEnemy[idx].GetComponent<Animator>().SetBool("isFly", true);
              //  }

                if (goEnemy[idx] != null)
                {
                    goEnemy[idx].transform.position = Vector3.MoveTowards(goEnemy[idx].transform.position, target.transform.position, Time.deltaTime * 5f);
                    
                }
            }
        }
    }

    float timerShowEnemy = 0;

    float timerLogin = 0;
    int countLogin = 5;

    float timerTutorial = 0;



	// Update is called once per frame
	void Update () {
	
     if(StatesManagement.state == 0){
			if (Input.GetKeyUp(KeyCode.Escape))
			{
				Application.Quit();
			}
			
			if (isClickPlay)
			{
				isClickPlay = false;
				//  gameover = 1;
				//PAUSE CANVAS
				stateM.setState(4);
				//  soundM.asMainMenu.Stop();
			}
		}else if (StatesManagement.state == 1){
			isLose = false;

			if (Input.GetKeyUp(KeyCode.Escape))
			{
				Application.LoadLevel(1);
			}
			
			if(!isLose){
				timerShowEnemy += Time.deltaTime;
				if (timerShowEnemy > 0.5f)
				{
					spownEnemy();
					timerShowEnemy = 0;
				}
				
				moveToEgg(0);
				moveToEgg(1);
				moveToEgg(2);
				moveToEgg(3);
				moveToEgg(4);
				moveToEgg(5);
				moveToEgg(6);
				moveToEgg(7);
				moveToEgg(8);
			}
			
			TouchEnemy();
			
	
			
			
			
		}else if (StatesManagement.state == 2){
			if (Input.GetKeyUp(KeyCode.Escape))
			{
				Application.LoadLevel(1);
			}
			timerShowEnemy += Time.deltaTime;
			if (timerShowEnemy > 3)
			{
				timerShowEnemy = 0;
				//  gameover = 4;
				
				//stateM.setState(0);
			}
		}else if(StatesManagement.state == 3){
			score = 0;
			life = 5;
		}else if(StatesManagement.state == 4){
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				//  isClickPlay = false;
				// gameover = 1;
				if(!isLose){
					stateM.setState(1);
				}else{
					stateM.setState(2);
				}
				//  soundM.asMainMenu.Stop();
			}
		} else if (StatesManagement.state == 5){
			if (Input.GetKeyUp(KeyCode.Escape))
			{
				Application.LoadLevel(1);
			}
			timerShowEnemy += Time.deltaTime;
			if (timerShowEnemy > 1)
			{
				timerShowEnemy = 0;
				//gameover = 3;
				
				
				//RESULT CANVAS
				
				stateM.setState(2);
				stateM.PlayResult();
				
				iplayer++;
				infoBestScore.SetActive(false);
				if (score > ibestScore)
				{
					ibestScore = score;
					infoBestScore.SetActive(true);
				}
				txYourScore.text = score.ToString();
				PlayerPrefs.SetInt("best", ibestScore);
				PlayerPrefs.SetInt("player", iplayer);
				//   txBestScore.text = ibestScore.ToString();
				//  txPlayer.text = iplayer.ToString();
			}
			if (isDestroyEnemy)
			{
				isDestroyEnemy = false;
				for (int n = 0; n < 9; n++)
				{
					if (goEnemy[n] != null)
					{
						condition[n] = 2;
					}
				}
			}
		}
		
		RespondDestroyEnemy(0);
		RespondDestroyEnemy(1);
		RespondDestroyEnemy(2);
		RespondDestroyEnemy(3);
		RespondDestroyEnemy(4);
		RespondDestroyEnemy(5);
		RespondDestroyEnemy(6);
		RespondDestroyEnemy(7);
		RespondDestroyEnemy(8);	
		
	}//UPDATE
	
	float timerShowResult = 0;
	void FixedUpdate()
	{
		txLife.text = "Life: " + life;
		txScore.text = "" + score;
	}
	
	
	
	
	private void PositiveTouch(Collider2D c2d)
	{
		if (c2d.name.Length > 6)
		{
			string idxEnemy = c2d.name.Substring(6, 1);
			try {
				int idx = int.Parse(idxEnemy);
				c2d.gameObject.GetComponent<Animator>().SetBool("isDead", true);
				condition[idx] = 2;
            } catch(FormatException)
            {

            }
            
        }
    }

    private void RespondDestroyEnemy(int idx)
    {
        if(condition[idx] == 2)
        {
            timerTouch[idx] += Time.deltaTime;
            if(timerTouch[idx] > 0.5f)
            {
                soundM.asNagaMati.Stop();
                if (!soundM.asNagaMati.isPlaying && StatesManagement.isPlayMusik == 1) soundM.asNagaMati.Play();
                timerTouch[idx] = 0;
                condition[idx] = 0;
                isShowEnemy[idx] = false;
               GameObject ps = (GameObject) Instantiate(particleDestroy, goEnemy[idx].transform.position, goEnemy[idx].transform.rotation);
                Destroy(ps, 1);
                Destroy(goEnemy[idx]);
                if(!isLose){
                score += 125;
                }
				txYourScore.text = score.ToString();
                // Debug.Log("KLSHFKLSHDKLFJS");
            }
        }
    }
	
	private void TouchEnemy()
	{	
        if (Input.touchCount > 0)
        { 
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch currentTouch = Input.GetTouch(i);
                if (currentTouch.phase == TouchPhase.Began)
                {
                    Vector2 v2 = new Vector2(Camera.main.ScreenToWorldPoint(currentTouch.position).x, Camera.main.ScreenToWorldPoint(currentTouch.position).y);
                    Collider2D c2d = Physics2D.OverlapPoint(v2);

                    if (c2d != null)
                    {
                     //   Debug.Log("ketek");
                        PositiveTouch(c2d);
                    }
                }
            }
        }
    }
}
