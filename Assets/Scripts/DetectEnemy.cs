﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class DetectEnemy : MonoBehaviour {

    public SoundManager soundM;
    public GameObject particleDestroy;
    public GameObject egg;
    //MenuUI gui;
    //public GameObject lv;
    //public GameObject j;
    //public Text k;
    //public GameObject l;
    //int score;
    // Use this for initialization
    void Start () {
		//gui = new MenuUI ();
	}

	int a = 5;
    private bool isPlayEffect = false;
    private float timerEffect = 0;
	// Update is called once per frame
	void Update () {
        if (Level1.gameover == 1)
       {
            PlayEffect();
        }
        
        if(StatesManagement.state == 1){
        	egg.SetActive(true);
        }
       
	}

    private float timerRed = 0;
    int changeColor = 0;
    private void PlayEffect()
    {
        if (isPlayEffect)
        {
            timerEffect += Time.deltaTime;
            if(timerEffect < 0.5f)
            {
                timerRed += Time.deltaTime;
                if(timerRed > 0.1f)
                {
                    changeColor++;

                    GetComponents<SpriteRenderer>()[0].color = (changeColor %2 == 0)? Color.red:Color.white;
                    timerRed = 0;
                }
            }
            else
            {
                GetComponents<SpriteRenderer>()[0].color = Color.white;
                isPlayEffect = false;
                timerEffect = 0;
            }
        }
    } 

    private void ResetPlayEffect()
    {
        isPlayEffect = true;
        timerEffect = 0;
    }

	//private void Finishing(){
	//	if (score > PlayerPrefs.GetInt ("Score")) {
	//		PlayerPrefs.SetInt ("Score", score);
	//		l.SetActive (true);
	//	} else {
	//		l.SetActive (false);
	//	}
	//	k.text = score.ToString();
	//	gui.EnableScore (j.GetComponent<Animator> ());
	//}

    void OnTriggerEnter2D(Collider2D col)
	{
        //  score = lv.GetComponent<Level1> ().score;
        //if (a == 0) {
        //	Finishing ();
        //} else {
        //	a--;
        if (Level1.life > 0)
        {
            Level1.life--;

        }
        else
        {

            Level1.life = 0;
			Level1.isLose = true;
            // Level1.gameover = 2;
            StatesManagement.state = 5;
            Level1.isDestroyEnemy = true;
            if (particleDestroy != null)
            {
                GameObject ps = (GameObject)Instantiate(particleDestroy, transform.position, transform.rotation);
                Destroy(ps, 1);
                
            }
            egg.SetActive(false);
        }

			DestroyEnemy(col.gameObject);
			ResetPlayEffect();
		//}
        
    }

    void DestroyEnemy(GameObject t)
    {
        if (t.name.Length < 6) return;
        string idxEnemy = t.name.Substring(6, 1);
        //Debug.Log("test deteksi " + t.name);
        int idx = int.Parse(idxEnemy);
        //Debug.Log(" idx " + idx);
        Level1.isShowEnemy[idx] = false;
        Destroy(t.gameObject);
    }
}
