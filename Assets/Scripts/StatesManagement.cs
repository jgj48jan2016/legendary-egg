﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class StatesManagement : MonoBehaviour {
    public GameObject gameplay;
    public GameObject mainmenu;
    public GameObject result;
    public GameObject tutorial;
    public GameObject pause;
    public GameObject credit;
    public GameObject btPause;
    public GameObject egg;
    public static int state = 0;
    // Use this for initialization

    public Sprite[] pathMusik;
    public Image srMusik;

    public Sprite[] pathMusikPS;
    public Image srMusikPS;
    public static int isPlayMusik = 1;

    SoundManager soundM;
    void Start () {
        isPlayMusik = PlayerPrefs.GetInt("best", 1);
        soundM = GetComponent<SoundManager>();


        if (isPlayMusik == 1)
        {
            srMusik.sprite = pathMusik[1];
            srMusikPS.sprite = pathMusikPS[1];
        }
        else
        {
            srMusik.sprite = pathMusik[0];
            srMusikPS.sprite = pathMusikPS[0];
        }

    }
	
	// Update is called once per frame
	void Update () {
		Debug.Log(state);
        switch (state)
        {
            case 0:
                if (!soundM.asMainMenu.isPlaying && isPlayMusik == 1) soundM.asMainMenu.Play();
                if (soundM.asMainMenu.isPlaying && isPlayMusik == 0) soundM.asMainMenu.Stop();
                break;
            case 1:
                if (!soundM.asGameplay.isPlaying && isPlayMusik == 1) soundM.asGameplay.Play();
                if (soundM.asGameplay.isPlaying && isPlayMusik == 0) soundM.asGameplay.Stop();
                break;
            case 4:
                if (!soundM.asGameplay.isPlaying && isPlayMusik == 1) soundM.asGameplay.Play();
                if (soundM.asGameplay.isPlaying && isPlayMusik == 0) soundM.asGameplay.Stop();
                break;
        }
	}

    public void setState(int state)
    {
        switch (state)
        {
            case 0: //MAINMENU
                StatesManagement.state = state;
                gameplay.SetActive(false);
                mainmenu.SetActive(true);
                result.SetActive(false);
                tutorial.SetActive(false);
                pause.SetActive(false);
                credit.SetActive(false);
                break;
            case 1: //GAMEPLAY
                StatesManagement.state = state;
                gameplay.SetActive(true);
                mainmenu.SetActive(false);
                result.SetActive(false);
                tutorial.SetActive(false);
                pause.SetActive(true);
                credit.SetActive(false);
                btPause.SetActive(true);
                if(!egg.activeSelf){
                	egg.SetActive(true);
                }
                soundM.asMainMenu.Stop();
                

               
                break;
            case 2: //RESULT
                StatesManagement.state = state;
                gameplay.SetActive(true);
                mainmenu.SetActive(false);
                result.SetActive(true);
                tutorial.SetActive(false);
                pause.SetActive(false);
                credit.SetActive(false);
                btPause.SetActive(false);
                soundM.asGameplay.Stop();
                if (!soundM.asScore.isPlaying && isPlayMusik == 1) soundM.asScore.Play();
                break;
            case 3: //TUTORIAL
                StatesManagement.state = state;
                gameplay.SetActive(true);
                mainmenu.SetActive(false);
                result.SetActive(false);
                tutorial.SetActive(true);
                pause.SetActive(false);
                credit.SetActive(false);
                soundM.asMainMenu.Stop();
                break;
            case 4: //PAUSE
                StatesManagement.state = state;
                gameplay.SetActive(true);
                mainmenu.SetActive(false);
                result.SetActive(false);
                tutorial.SetActive(false);
                pause.SetActive(true);
                credit.SetActive(false);
                btPause.SetActive(false);
                if (!soundM.asGameplay.isPlaying && isPlayMusik == 1)
                    soundM.asGameplay.Pause();
                break;
            case 7: //CREDIT
                gameplay.SetActive(false);
                mainmenu.SetActive(false);
                result.SetActive(false);
                tutorial.SetActive(false);
                pause.SetActive(false);
                credit.SetActive(true);
                break;
        }
    }

    public void OnClickPause()
    {
        setState(4);
        pause.GetComponent<Animator>().SetBool("isPaused",true);
    }

    public void OnClickContinue()
	{	
		
		if(!Level1.isLose){
		pause.GetComponent<Animator>().SetBool("isPaused",false);
		setState(1);
		}else{
			setState(2);
		}
		
    }

    public void OnClickCloseTutorial()
    {
        setState(1);
    }

    public void OnClickCloseCredit()
    {
        setState(0);
    }

    public void OnClickCredit()
    {
        setState(7);
    }

    public void OnClickMusik()
    {
        if(isPlayMusik == 0)
        {
            isPlayMusik = 1;
            srMusik.sprite = pathMusik[1];
        }
        else
        {
            isPlayMusik = 0;
            srMusik.sprite = pathMusik[0];
        }
        PlayerPrefs.SetInt("best", isPlayMusik);

    }

    public void OnClickMusikGameplay()
    {
        if (isPlayMusik == 0)
        {
            isPlayMusik = 1;
            srMusikPS.sprite = pathMusikPS[1];
        }
        else
        {
            isPlayMusik = 0;
           srMusikPS.sprite = pathMusikPS[0];
        }
        PlayerPrefs.SetInt("best", isPlayMusik);

    }

    public void PlayAgain()
    {
        //state = 0;
        Application.LoadLevel(1);
        setState(1);
    }
    
    public void PlayResult(){
    	result.GetComponent<Animation>().Play();
    }

    
}
